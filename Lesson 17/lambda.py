def squared(num):
    return num * num


# lamnda : num : num * num

print(squared(2))

addTwo = lambda num: num + 2
# lambda num: num + 2

print(addTwo(2))

sum_totoal = lambda a, b: a + b
# lambda a, b: a + b

print(sum_totoal(2, 1))

#################################


def funcBuilder(x):
    return lambda num: num + x


addTen = funcBuilder(10)
addTwenty = funcBuilder(20)


print(addTen(7))
print(addTwenty(7))

##########################


numbers = [2, 78, 12, 18, 20, 21]

squared_nums = map(lambda num: num * num, numbers)

print(list(squared_nums))

####################################


odd_nums = filter(lambda num: num % 2 != 0, numbers)
print(list(odd_nums))


######################################################


from functools import reduce

lambda acc, curr: acc + curr

numbers = [1, 2, 3, 4, 5]

total = reduce(lambda acc, curr: acc + curr, numbers)

print(total)

print(sum(numbers, 10))

lambda acc, curr: acc + len(curr)

names = ["Dave", "Sara", "John", "ABD"]

char_count = reduce(lambda acc, curr: acc + len(curr), names, 0)

print("----------------")
print(char_count)
