class Vehicle:
    def __init__(self, make, model):
        self.make = make
        self.model = model

    def moves(self):
        print("Moves along..")

    def get_make_model(self):
        print(f"I'm a {self.make} {self.model}.")


my_car = Vehicle("Tesla", "Model 3")

# print(my_car.make)
# print(my_car.model)
my_car.get_make_model()
my_car.moves()

your_car = Vehicle("Toyota", "Fortuner")
your_car.get_make_model()
your_car.moves()


class Aeroplane(Vehicle):
    def __init__(self, make, model, faa_id):
        super().__init__(make, model)
        self.faa_id = faa_id

    def moves(self):
        print("Flies along....")


class Truck(Vehicle):
    def moves(self):
        print("Rumbles along ....")


class GolfCart(Vehicle):
    pass


indigo = Aeroplane("Boing", "747", "N-12345")
truck = Truck("Tata", "Magic")
golf_car = GolfCart("Yamaha", "GC100")


indigo.get_make_model()
indigo.moves()
truck.get_make_model()
truck.moves()
golf_car.get_make_model()
golf_car.moves()

print("\n\n")

for v in (my_car, your_car, indigo, truck, golf_car):
    v.get_make_model()
    v.moves()
